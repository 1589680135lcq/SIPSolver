// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_SEMIINFINITEPROGRAM_H_
#define SIP_SOLVE_SEMIINFINITEPROGRAM_H_

#include <vector>
#include "types.h"
#include "BasisElt.h"
#include "DampedPartialFractionVector.h"

using std::vector;

// A semi-infinite program is the problem:
//
// minimize \sum_n y_n pointObj[n] over
//
// y_n >= 0 and y_{m,x} >= 0, such that
//
// \sum_n y_n points[n] + \sum_{m,x} y_{m,x} curves[m](x) = norm
//
class SemiInfiniteProgram {
 public:
  // The right hand side of our equality constraint (for example,
  // minus the unit vector)
  vector<Real> norm;

  // A collection of point vectors (for example, isolated operators
  // with a gap in dimension, or slack vectors in a feasibility
  // problem)
  vector<vector<Real> > points;

  // The value of the objective function for the coefficients of our
  // point vectors. We do not currently allow curve vectors to have
  // nonzero objective function
  vector<Real> pointObj;

  // A collection of curves.  Each curve is a
  // DampedPartialFractionVector, which is a vector where each
  // component has the form (rho)^x * (rational function of x)
  vector<DampedPartialFractionVector> curves;

  inline int dim() const { return norm.size(); }

  // The vector associated with the given basis element.  For a point
  // vector, we look up the answer in points, for a curve vector, we
  // evaluate the given curve at the given argument
  vector< Real> vec(const BasisElt &l) const {
    if (l.ty == SIP_POINT)
      return points[l.n];
    else
      return curves[l.n](l.x);
  }

  // The value of the objective function associated to the given basis
  // element.  Only point vectors have nonzero objective function
  Real obj(const BasisElt &l) const {
    if (l.ty == SIP_POINT)
      return pointObj[l.n];
    else
      return 0;
  }
};

#endif  // SIP_SOLVE_SEMIINFINITEPROGRAM_H_
