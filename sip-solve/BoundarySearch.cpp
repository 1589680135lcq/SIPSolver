// Author(s): D. Simmons-Duffin, April-October 2013

#include <iostream>
#include <fstream>
#include <algorithm>
#include <utility>
#include <string>
#include <vector>
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/function.hpp"
#include "boost/phoenix.hpp"
#include "boost/phoenix/core.hpp"
#include "boost/phoenix/core/reference.hpp"
#include "boost/phoenix/function.hpp"
#include "boost/phoenix/bind/bind_function.hpp"
#include "boost/serialization/utility.hpp"
#include "BranchAndBound.h"
#include "parse.h"
#include "SIPSolver.h"
#include "tests.h"
#include "omp.h"
#include "stdlib.h"
#include "serialize.h"
#include "util.h"
#include "BoundarySearch.h"

using std::string;
using std::endl;
using std::cout;
using boost::filesystem::path;
using boost::filesystem::exists;
using boost::archive::text_oarchive;
using boost::phoenix::bind;
using boost::phoenix::ref;
using boost::phoenix::val;
using boost::phoenix::arg_names::arg1;

void printBoundarySearchInfo(Real xMid, Real xMin, Real xMax,
                             vector<BasisElt> basis) {
  cout << "\n\n=======================================================" << endl;
  cout << "Bounded x <- [" << xMin << ", " << xMax << "]" << endl;
  cout << "Trying x = " << xMid << endl;
  cout << "Current feasible basis: " << endl;
  printBasis(cout, basis);
}

pair<Real, vector< pair<BasisElt, Real> > >
BoundarySearch::run(SemiInfiniteProgram sip,
                    Real thresh,
                    const boost::function<void(const BoundarySearch&)> &saveCheckpoint) {
  while (true) {
    saveCheckpoint(*this);

    // If we haven't yet found a feasible basis, do that first
    if (feasibleBasis.size() == 0)
      findFeasibleBasis(sip, saveCheckpoint);

    // A termination condition.  If finished, return the current
    // feasible basis and coefficients
    if (abs(xMax - xMin) < thresh) {
      vector< pair<BasisElt, Real> > basis;
      for (unsigned int i = 0; i < feasibleCoords.size(); i++) {
        basis.push_back(pair<BasisElt, Real>(feasibleBasis[i], feasibleCoords[i]));
      }
      return pair<Real, vector< pair<BasisElt, Real> > >(xMin, basis);
    }
    // Otherwise, choose a point xMid between xMin and xMax and try to
    // find a new feasible basis with scalar dimensions above xMid.

    // Choose xMid closer to xMin by an amount dertermined by the time
    // it takes to test a feasible point vs. an infeasible one.  Naive
    // bisection is a poor idea here because infeasible points usually
    // take much longer
    Real xMid =
      (xMax + INFEASIBLE_TO_FEASIBLE_TIME_RATIO * xMin) /
      (1 + INFEASIBLE_TO_FEASIBLE_TIME_RATIO);

    printBoundarySearchInfo(xMid, xMin, xMax, feasibleBasis);

    // Try to minimize the total sum of OPE coefficients for operators
    // with dimensions below xMid.
    SIPSolver solver = minimizeOPECoeffsBelow(sip, xMid);

    Real obj = runCheckpointedSolver(&solver, MAX_ITERATIONS, saveCheckpoint);
    if (obj > 0) {
      xMax = xMid;
    } else {
      xMin = xMid;
      feasibleBasis  = solver.basis;
      feasibleCoords = solver.coefficients();
      workingBasis.clear();
    }
  }
}

// A new SIP for the purpose of minimizing the total OPE
// coefficients of vectors with dimension below xMinNew
SIPSolver BoundarySearch::minimizeOPECoeffsBelow(const SemiInfiniteProgram &oldSIP,
                                                 const Real &xMinNew) {
  SemiInfiniteProgram sip = oldSIP;

  // Restrict scalars to have dimension above xMinNew
  sip.curves[0].xLeft = xMinNew;

  // Here, we find any curve elements of feasibleBasis whose
  // dimensions are less than xMinNew.  These members are turned into
  // point elements, tacked onto the end of sip.points, and assigned a
  // positive objective function, which we will try to minimize.
  vector<BasisElt> initialBasis;
  for (unsigned int i = 0; i < feasibleBasis.size(); i++) {
    BasisElt b = feasibleBasis[i];

    if (b.ty == SIP_CURVE && b.n == 0 && b.x < xMinNew) {
      sip.points.push_back(sip.curves[0](b.x));
      sip.pointObj.push_back(1);
      initialBasis.push_back(BasisElt::point(sip.points.size() - 1));
    } else {
      initialBasis.push_back(b);
    }
  }

  // If a previously saved working basis is available, use that
  // instead of initialBasis
  return SIPSolver(sip, workingBasis.size() > 0 ? workingBasis : initialBasis);
}

void BoundarySearch::findFeasibleBasis(const SemiInfiniteProgram &sip,
                                       const BoundarySearchSaver &saveBoundarySearch) {
  SIPSolver solver = SIPSolver::feasibility(sip, workingBasis);
  solver.sip.curves[0].xLeft = xMin;
  cout << "Determining initial feasible solution..." << endl;
  Real obj = runCheckpointedSolver(&solver, MAX_ITERATIONS, saveBoundarySearch);
  if (obj <= 0) {
    cout << "Found initial feasible solution" << endl;
    feasibleBasis  = solver.basis;
    feasibleCoords = solver.coefficients();
    workingBasis.clear();
  } else {
    throw("Couldn't find initial feasible solution");
  }
}

void saveBoundarySearchCheckpoint(text_oarchive &checkpointArchive,
                                  boost::filesystem::ofstream &checkpoints,
                                  const BoundarySearch& b) {
  cout << "Saving checkpoint...";
  checkpointArchive << b;
  checkpoints.flush();
  cout << " done." << endl;
}

void runSIPFileBoundarySearch(path sipPath,
                              path outPath,
                              path checkpointPath,
                              int searchIndex,
                              Real xMin,
                              Real xMax,
                              Real thresh) {
  SemiInfiniteProgram sip = readSemiInfiniteProgram(sipPath.c_str());
  cout << "Computing scalar boundary for: " << sipPath << endl;
  cout << "Outputting result to         : " << outPath << endl;
  cout << "Looking for checkpoints in   : " << checkpointPath << endl;
  vector<BoundarySearch> boundarySearches = loadManyFromArchive<BoundarySearch>(checkpointPath);

  if (exists(checkpointPath)) {
    path backup = backupFile(checkpointPath);
    cout << "Backed up old checkpoints to : " << backup << endl;
  }
  cout << "Using " << omp_get_max_threads() << " threads" << endl;
  cout << "Found " << boundarySearches.size() << " checkpoints" << endl;

  boost::filesystem::ofstream checkpoints(checkpointPath);
  text_oarchive checkpointArchive(checkpoints);
  const boost::function<void(const BoundarySearch&)> saveCheckpoint =
    bind(&saveBoundarySearchCheckpoint, ref(checkpointArchive), ref(checkpoints), arg1);

  if (boundarySearches.size() > 0) {
    // Load all but the last into the archive again (the last will be
    // stored again as soon as the search is started)
    for (unsigned int i = 0; i < boundarySearches.size() - 1; i++)
      checkpointArchive << boundarySearches[i];
  } else {
    boundarySearches.push_back(BoundarySearch(xMin, xMax));
  }

  // Run starting with the last boundary finder
  pair<Real, vector< pair<BasisElt, Real> > > bdry = boundarySearches.back().run(sip, thresh, saveCheckpoint);
  checkpoints.close();

  cout << "\n*******************************************************" << endl;
  cout << "Finished boundary search" << endl;
  printBoundaryJson(cout, bdry);

  boost::filesystem::ofstream outStream(outPath);
  outStream.precision(30);
  printBoundaryJson(outStream, bdry);
  outStream.close();
}
