// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_BASISELT_H_
#define SIP_SOLVE_BASISELT_H_

#include <iostream>
#include <sstream>
#include <vector>
#include "types.h"

using std::vector;
using std::ostream;

// There are two types of vectors in a semi-infinite program.
// Individual vectors (for example conformal blocks with a dimension
// gap on both sides) are called 'point vectors' and have type
// SIP_POINT.  Vectors which lie on a continuous curve (for example,
// conformal blocks which lie in the middle of a continuum of
// dimensions) are called 'curve vectors' and have type SIP_CURVE.
enum BasisEltType { SIP_POINT, SIP_CURVE };

// An object representing a vector in a semi-infinite program which
// can be part of our basis.  A point is labeled by an index n, while
// a curve vector is labeled by an index n (indicating which curve
// it's on) and also an argument x (indicating where on that curve it
// lies).
class BasisElt {
 public:
  // A type -- either SIP_POINT or SIP_CURVE
  BasisEltType ty;

  // An index indicating which point (for SIP_POINT) or which curve
  // for (SIP_CURVE)
  int n;

  // An argument indicating where on the curve this BasisElt lies
  // (only makes sense for SIP_CURVE)
  Real x;

  static inline BasisElt curve(int n, Real x) {
    return BasisElt(SIP_CURVE, n, x);
  }
  static inline BasisElt point(int n) {
    return BasisElt(SIP_POINT, n, 0);
  }

  friend bool operator<(const BasisElt &a, const BasisElt &b);
  friend ostream& operator<<(ostream& os, const BasisElt& b);

  // A public default constructor is needed for serialization
  BasisElt() {}

 private:
  BasisElt(BasisEltType ty, int n, Real x): ty(ty), n(n), x(x) {}
};

ostream& printBasis(ostream& os, const vector<BasisElt> &basis);

#endif  // SIP_SOLVE_BASISELT_H_
