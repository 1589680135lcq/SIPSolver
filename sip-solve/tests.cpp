// Author(s): D. Simmons-Duffin, April-October 2013

#include <vector>
#include <fstream>
#include "boost/archive/text_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "types.h"
#include "parse.h"
#include "SIPSolver.h"
#include "ParabolaSearch.h"
#include "serialize.h"

void printVector(vector<Real> v) {
  unsigned int i;
  for (i = 0; i < v.size(); i++) {
    cout << v[i];
    if (i < v.size() - 1)
      printf(", ");
  }
}

void printArray(int n, Real *v) {
  printf("{");
  for (int i = 0; i < n; i++) {
    cout << v[i];
    if (i < n - 1)
      printf(", ");
  }
  printf("}");
}

void printMatrix(int n, int m, Real * A) {
  printf("{");
  for (int i = 0; i < n; i++) {
    printArray(m, A+i*m);
    if (i < n - 1)
      printf(",\n");
  }
  printf("}\n");
}

void parsingAndMathTest(const char* sipFile) {
  cout << "reading...\n";
  SemiInfiniteProgram sip = readSemiInfiniteProgram(sipFile);
  cout << "done\n";
  printf("components: \n");
  printVector(sip.curves[0].components[0].p0.coeffs);
  printf("\n");
  printVector(sip.curves[0].components[1].p0.coeffs);
  printf("\n");
  printVector(sip.curves[0].components[2].p0.coeffs);
  printf("\n");
  printf("dotted with sip.norm: \n");
  printVector(sip.curves[0].dot(sip.norm).p0.coeffs);
  printf("\n");
  cout << "numerator evaluated at 1: " << sip.curves[0].dot(sip.norm)(Real(1)) << "\n";
  cout << "rational evaluated at 2.4: " << sip.curves[0].dot(sip.norm)(Real(2.4)) << "\n";
}

void psTest() {
  Polynomial p0;
  Real cs[] = {-8.13179609310391, -5.847694793548868, 2.0830447402080274,
               -4.328305289105632, 5.852313067159289, 4.944744302154783,
               -0.4434059596902218, -0.4154362356822894, -2.1925899664173643,
               1.3555048306631852, 0.21735621733317412};
  p0.coeffs.insert(p0.coeffs.end(), cs, cs + 11);
  DampedPartialFraction f(1, p0, vector<Real>(), vector<Real>(), -10, 10);
  ParabolaSearch<DampedPartialFraction> ps(1e-10);
  vector<DampedPartialFraction> vp;
  vp.push_back(f);
  boost::tuple<int, Real, Real> min = ps.minimizeFunctions(vp, 1e10);
  cout << "iMin: " << min.get<0>() << "\n";
  cout << "xMin: " << min.get<1>() << "\n";
  cout << "fMin: " << min.get<2>() << "\n";
}

void polTest() {
  Polynomial p0;
  Real cs[] = {-8.13179609310391, -5.847694793548868, 2.0830447402080274,
               -4.328305289105632, 5.852313067159289, 4.944744302154783,
               -0.4434059596902218, -0.4154362356822894, -2.1925899664173643,
               1.3555048306631852, 0.21735621733317412};
  p0.coeffs.insert(p0.coeffs.end(), cs, cs + 11);
  interval<Real> i = p0(interval<Real>(1.234, 5.678));
  cout << i.lower() << ", " << i.upper();
}

// Correct output:
//
// f(2.71828): 1451.58948242616498641772043527
// f.derivative1(2.71828): 4297.82064920365081931494146862
// f.derivative2(2.71828): 10808.5589690950041209994677512
// f.derivative3(2.71828): 21691.6487658861438880446945381
// f.derivative3([2.71828,3.14159]): 13203.1417834191321881916140535, 48554.8475585085959666017913156
// v.maxDegree(): 10
// f3(2.71828): 65899.7157395249005911577948899
// f3.derivative1(2.71828): 52461.3735082964296021719701357
// f3.derivative2(2.71828): 115487.30882739391835936093009
// f3.derivative3(2.71828): 272965.817998444884185432991656
//
void dprTest() {
  Polynomial p0;
  Real cs[] = {-8.13179609310391, -5.847694793548868, 2.0830447402080274,
               -4.328305289105632, 5.852313067159289, 4.944744302154783,
               -0.4434059596902218, -0.4154362356822894, -2.1925899664173643,
               1.3555048306631852, 0.21735621733317412};
  p0.coeffs.insert(p0.coeffs.end(), cs, cs + 11);
  Real as[] = {-1, -2, -3.14159};
  Real rs[] = {10, 40, -100};
  vector<Real> poles(as, as+3);
  vector<Real> residues(rs, rs+3);
  DampedPartialFraction f(0.5, p0, poles, residues);
  cout << "f(2.71828): " << f(Real(2.71828)) << "\n";
  cout << "f.derivative1(2.71828): " << f.derivative1(Real(2.71828)) << "\n";
  cout << "f.derivative2(2.71828): " << f.derivative2(Real(2.71828)) << "\n";
  cout << "f.derivative3(2.71828): " << f.derivative3(Real(2.71828)) << "\n";
  interval<Real> i = f(interval<Real>(2.71828, 3.14159));
  cout << "f([2.71828,3.14159]): " << i.lower() << ", " << i.upper() << "\n";

  Real cs2[] = {1000, 2000, 3000};
  Polynomial p2;
  p2.coeffs.insert(p2.coeffs.end(), cs2, cs2+3);
  Real rs2[] = {-9800, -300, 4000};
  vector<Real> residues2(rs2, rs2+3);
  DampedPartialFraction f2(0.5, p2, poles, residues2);

  DampedPartialFractionVector v;
  v.components.push_back(f);
  v.components.push_back(f2);
  cout << "v.maxDegree(): " << v.maxDegree() << "\n";
  DampedPartialFraction f3 = v.dot(vector<Real>(2, 12));
  cout << "f3(2.71828): " << f3(Real(2.71828)) << "\n";
  cout << "f3.derivative1(2.71828): " << f3.derivative1(Real(2.71828)) << "\n";
  cout << "f3.derivative2(2.71828): " << f3.derivative2(Real(2.71828)) << "\n";
  cout << "f3.derivative3(2.71828): " << f3.derivative3(Real(2.71828)) << "\n";
}

void serializeTest() {
  std::ofstream ofs("test.ar");
  boost::archive::text_oarchive oa(ofs);
  BasisElt b1 = BasisElt::curve(1, Real("123.456"));
  BasisElt b2 = BasisElt::curve(1, Real("789.101"));
  Real r1 = Real(1)/Real(3);
  Real r2 = Real("2.89402321898191677371201966356018195295130937972409032078763117083177e-1");
  oa << b1;
  oa << b2;
  oa << r1;
  oa << r2;

  ofs.close();

  vector<BasisElt> cs;
  Real q1;
  Real q2;
  BasisElt c1;
  BasisElt c2;
  std::ifstream ifs("test.ar");
  boost::archive::text_iarchive ia(ifs);
  ia >> c1;
  ia >> c2;
  ia >> q1;
  ia >> q2;

  cout << c1 << "\n";
  cout << c2 << "\n";
  cout << "r1 == r1_serialized: " << (r1 == q1) << "\n";
  cout << "r2 == r2_serialized: " << (r2 == q2) << "\n";
  cout << r1 << "\n";
  cout << q1 << "\n";
  cout << r2 << "\n";
  cout << q2 << "\n";

  ifs.close();
}
