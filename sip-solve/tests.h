// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_TESTS_H_
#define SIP_SOLVE_TESTS_H_

#include <vector>
#include "types.h"

void printVector(vector<Real> v);

void parsingAndMathTest(const char *);
void psTest();
void dpTest();
void polTest();
void dprTest();
void serializeTest();

#endif  // SIP_SOLVE_TESTS_H_
