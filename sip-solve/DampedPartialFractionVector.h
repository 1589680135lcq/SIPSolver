// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_DAMPEDPARTIALFRACTIONVECTOR_H_
#define SIP_SOLVE_DAMPEDPARTIALFRACTIONVECTOR_H_

#include <vector>
#include "types.h"
#include "DampedPartialFraction.h"

class DampedPartialFractionVector {
 public:
  vector<DampedPartialFraction> components;
  Real xLeft;
  Real xRight;
  vector<Real> operator()(const Real &x) const;
  int maxDegree() const;
  DampedPartialFraction dot(const vector<Real> &v) const;
};

#endif  // SIP_SOLVE_DAMPEDPARTIALFRACTIONVECTOR_H_
