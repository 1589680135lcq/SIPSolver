// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_POLYNOMIAL_H_
#define SIP_SOLVE_POLYNOMIAL_H_

#include <vector>
#include "boost/numeric/interval.hpp"
#include "types.h"

using std::vector;
using boost::numeric::interval;

class Polynomial {
 public:
  vector<Real> coeffs;

  int degree() const {
    return coeffs.size() - 1;
  };

  Real operator()(const Real &x) const {
    int deg = degree();
    Real y = coeffs[deg];
    for (int i = deg - 1; i >= 0; i--) {
      y *= x;
      y += coeffs[i];
    }
    return y;
  }

  interval<Real> operator()(const interval<Real> &x) const {
    interval<Real> y(0, 0);
    int deg = degree();
    for (int i = deg; i >= 0; i--) {
      y *= x;
      y += coeffs[i];
    }
    return y;
  }

  Polynomial derivative() const {
    Polynomial p;
    int deg = degree();
    p.coeffs.resize(deg);

    for (int i = 1; i <= deg; i++)
      p.coeffs[i - 1] = i*coeffs[i];

    return p;
  }
};

#endif  // SIP_SOLVE_POLYNOMIAL_H_
