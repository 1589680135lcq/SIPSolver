// Author(s): D. Simmons-Duffin, April-October 2013

#ifndef SIP_SOLVE_PARSE_H_
#define SIP_SOLVE_PARSE_H_

#include "SIPSolver.h"

SemiInfiniteProgram readSemiInfiniteProgram(const char*);

#endif  // SIP_SOLVE_PARSE_H_
