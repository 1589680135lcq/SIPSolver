#!/bin/sh

# Be sure to run 'make' in the sip-solve directory before trying to
# run this script

# The input file is a semi-infinite program in an xml format.  The
# semi-infinite program includes a list of "curves", which are vector
# functions of delta. In the case of a singlet dimension bound, we
# have a curve for each spin L=0,2,4,....
SIPFILE=singletBound-d3-deltaPhi0.518-Lmax30-nmax10.xml

# The index (L/2) of the curve on which to do bisection.  Currently only the
# value 0 (L=0) is supported.
SEARCHINDEX=0

# The search occurs in the gap of the lowest dimension operator above
# L+1.  We'll start at 0 (corresponding to Delta=1 for scalars).
SEARCHMIN=0

# We'll search up to a gap of 1 (Delta=2).
SEARCHMAX=1

# We'll search to within 10^-2 of the boundary
SEARCHTHRESH=1e-2

../sip-solve/sip-solve $SIPFILE search $SEARCHINDEX $SEARCHMIN $SEARCHMAX $SEARCHTHRESH
