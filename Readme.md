A semi-infinite program solver implemented for
https://arxiv.org/abs/1403.4545. See the paper for a description of
the algorithm. This is one of two solvers implemented for that
paper. The other is implemented in Python/Cython.
