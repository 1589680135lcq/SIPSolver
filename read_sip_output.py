#!/bin/env python

import json
import re
import sys

def read_sip_outfile(path):
    with open(path, 'r') as f:
        return json.load(f)

def get_delta_phi(path):
    return float(re.match('.*deltaPhi([0-9.]+)-.*', path).group(1))

def get_sorted_data(paths):
    data = [(get_delta_phi(p), read_sip_outfile(p)) for p in paths]
    data.sort(key=lambda d: d[0])
    return data

def extract_dimensions(basis, l):
    spin_l_ops = (o for o in basis if o['index'] == l/2 and o['type'] == 'curve')
    dimensions = [o['x'] for o in spin_l_ops]
    dimensions.sort()
    return dimensions

def math_list(xs):
    return '{' + ','.join(xs) + '}'

def dimension_bound_string(data):
    return math_list(math_list((str(deltaPhi), str(spectrum['xBoundary'])))
                     for deltaPhi, spectrum in data) 

def spin_l_spectrum(l, data):
    return math_list(math_list(math_list((str(deltaPhi), str(dim)))
                               for dim in extract_dimensions(spectrum['basis'], l))
                     for deltaPhi, spectrum in data)

def wrap_basis(basis):
    return { 'basis': basis }

def main(argv):
    data = get_sorted_data(argv[1:])
    data = [(deltaPhi, wrap_basis(basis)) for deltaPhi, basis in data]
    #print dimension_bound_string(data)
    for l in range(0, 20, 2):
        print 'spectrumCMinimizationNmax10Spin[%s] = %s;\n' % (l, spin_l_spectrum(l, data))

if __name__ == "__main__":
    main(sys.argv)

